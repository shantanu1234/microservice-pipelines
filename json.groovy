import groovy.json.JsonSlurper 

class Example {
   static void main(String[] args) {
      def jsonSlurper = new JsonSlurper()
      def object = jsonSlurper.parseText('{ "EmployeeID":11,"EmployeeName":"Shantanu"}') 

      println(object.EmployeeID);
      println(object.EmployeeName);
   } 
}
